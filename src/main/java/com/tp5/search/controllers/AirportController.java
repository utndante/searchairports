package com.tp5.search.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Controller
public class AirportController {

    @Autowired
    RestTemplate restTemplate;

    private String baseUrl = "http://127.0.0.1:8080";

    @GetMapping("/airports")
    public ResponseEntity getAirports() {
        String url = baseUrl + "/countries/1/states/1/cities/1/airports";
        ResponseEntity<Object[]> responseEntity;

        try {
            responseEntity = restTemplate.getForEntity(url, Object[].class);
        } catch (RestClientException e) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.toString());
        }

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("algo salio mal");
        }

        return ResponseEntity.status(HttpStatus.OK).body(responseEntity.getBody());
    }

    @GetMapping(value = "/routes")
    public ResponseEntity index(@RequestParam(value = "origin", required = false) Long origin, @RequestParam(value = "destination", required = false) Long destination) {
        String requestUrl = baseUrl + "/routes";
        ResponseEntity<Object[]> responseEntity;

        if (origin != null && destination != null) {
            requestUrl = requestUrl + "?origin=" + origin + "&destination=" + destination;
        } else if (origin != null) {
            requestUrl = requestUrl + "?origin=" + origin;
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("dame bien las cosas");
        }

        try {
            System.out.println("HACIENDO REQUEST A: " + requestUrl);
            responseEntity = restTemplate.getForEntity(requestUrl, Object[].class);
        } catch (RestClientException e) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.toString());
        }

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("algo salio mal");
        }

        return ResponseEntity.status(HttpStatus.OK).body(responseEntity.getBody());
    }

}
